import 'package:flutter/material.dart';

import 'package:monalisa/artwork_widget.dart';

class MuseumApp extends StatelessWidget {
  const MuseumApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Museum',
      theme: ThemeData(
        primarySwatch: Colors.brown,
        textTheme: const TextTheme(
          displayLarge: TextStyle(
            fontSize: 30,
            fontFamily: "Merriweather",
            fontWeight: FontWeight.bold,
          ),
          displayMedium: TextStyle(
            fontSize: 15,
            fontFamily: "Merriweather",
          ),
          displaySmall: TextStyle(
            color: Colors.black,
            fontSize: 16,
          ),
        ),
      ),
      home: const ArtworkWidget(),
    );
  }
}
