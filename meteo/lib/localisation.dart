import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class LocalisationWidget extends StatefulWidget {
  final Function(double, double)? onLocationObtained;
  const LocalisationWidget({super.key, this.onLocationObtained});

  @override
  State<LocalisationWidget> createState() => _LocationWidgetState();
}

class _LocationWidgetState extends State<LocalisationWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          ElevatedButton(
            onPressed: _determinePosition,
            child: const Text('Get Location'),
          ),
        ],
      ),
    );
  }

  Future<void> _determinePosition() async {
    Position position = await Geolocator.getCurrentPosition();
    if (widget.onLocationObtained != null) {
      widget.onLocationObtained!(position.latitude, position.longitude);
    }
  }
}
